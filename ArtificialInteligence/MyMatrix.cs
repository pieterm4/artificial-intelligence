﻿using System.Collections.Generic;

namespace ArtificialInteligence
{
    public class MyMatrix
    {
        public static double[,] Multiply(double[,] matrix1, double[,] matrix2)
        {
            double[,] newMatrix = new double[matrix1.GetLength(0), matrix2.GetLength(1)];

            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix2.GetLength(1); j++)
                {
                    double s = 0;
                    for (int k = 0; k < matrix2.GetLength(1); k++)
                    {
                        s += matrix1[i, k] * matrix2[k, j];
                    }
                    newMatrix[i, j] = s;

                }
            }

            return newMatrix;
            
        }

        public static int[,] Multiply(int[,] matrix1, int[,] matrix2)
        {
            int[,] newMatrix = new int[matrix1.GetLength(0), matrix2.GetLength(1)];

            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix2.GetLength(1); j++)
                {
                    int s = 0;
                    for (int k = 0; k < matrix2.GetLength(1); k++)
                    {
                        s += matrix1[i, k] * matrix2[k, j];
                    }
                    newMatrix[i, j] = s;

                }
            }

            return newMatrix;

        }

        public static double Multiply(double[] matrix1, double[] matrix2)
        {
            
                double s = 0;
                for (int i = 0; i < matrix1.GetLength(0); i++)
                {
                    s += matrix1[i] * matrix2[i];
                }

                return s;
           
            
        }

        public static double[] Multiply(double[] matrix, double number)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                matrix[i] *= number;
            }
            return matrix;
        }

        public static double[] Substract(double[] matrix1, double[] matrix2)
        {
            if (matrix1 != null && matrix2 != null)
            {
                double[] wynik = new double[matrix1.GetLength(0)];

                for (int i = 0; i < matrix1.GetLength(0); i++)
                {
                    wynik[i] = matrix1[i] - matrix2[i];
                }

                return wynik;
            }
            else
            {
                return null;
            }
        }
        public static double[] Add(double[] matrix1, double[] matrix2)
        {
            if (matrix1 != null && matrix2 != null)
            {
                double[] wynik = new double[matrix1.GetLength(0)];

                for (int i = 0; i < matrix1.GetLength(0); i++)
                {
                    wynik[i] = matrix1[i] + matrix2[i];
                }

                return wynik;
            }
            else
            {
                return null;
            }
        }

        public static double Normalize(List<double[]> dane, int newMin, int newMax, double value)
        {
            double min = GetMin(dane);
            double max = GetMax(dane);

            double newValue = (value - min) / (max - min) * (newMax - newMin) + newMin;

            return newValue;
        }

        private static double GetMin(List<double[]> dane)
        {
            double min = dane[0][0];

            foreach (var doublese in dane)
            {
                for (int i = 0; i < doublese.GetLength(0); i++)
                {
                    if (doublese[i] < min)
                        min = doublese[i];
                }
            }

            return min;
        }
        private static double GetMax(List<double[]> dane)
        {
            double max = dane[0][0];

            foreach (var doublese in dane)
            {
                for (int i = 0; i < doublese.GetLength(0); i++)
                {
                    if (doublese[i] > max)
                        max = doublese[i];
                }
            }

            return max;
        }


    }
}

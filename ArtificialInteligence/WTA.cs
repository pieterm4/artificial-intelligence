﻿using System;
using System.Collections.Generic;

namespace ArtificialInteligence
{
    public class WTA
    {
        public int NumberOfNeurals { get; set; }
        public int AmountOfNEuralEntries { get; set; }
        public double Alpha { get; set; }
        public List<Neural> Neurals { get; set; }
        public Random Random { get; set; }
        public List<double[]> Outputs { get; set; }


        public WTA(int _epochs, int _numberOfNeurals, int _amountOfEntries, double alpha)
        {
            Random = new Random(Guid.NewGuid().GetHashCode());
            NumberOfNeurals = _numberOfNeurals;
            AmountOfNEuralEntries = _amountOfEntries;
            Alpha = alpha;

            List<Neural> neurals = new List<Neural>();
            for (int i = 0; i < NumberOfNeurals; i++)
            {
                neurals.Add(new Neural(Alpha, InitialWeights()));
            }

            Neurals = new List<Neural>(neurals);
            
        }

        private double[] InitialWeights()
        {
            double[] weights = new double[AmountOfNEuralEntries];
            for (int i = 0; i < AmountOfNEuralEntries; i++)
            {
               
                weights[i] = Random.NextDouble();
            }

            return weights;
        }
        //Learn
        public void Learn(int epochs, List<double[]> data)
        {
            for (int i = 0; i < epochs; i++)
            {
                for (int j = 0; j < data.Count; j++)
                {
                    //List<int> indexes = new List<int>();
                    int maxIndex = -1;
                    if (j % 5 != 0)
                    {
                        var tmp = data[j];
                        maxIndex = BestNeural(tmp, Neurals);
                        Neurals[maxIndex].Aktualizuj(tmp);


                    }
                }
            }
        }

        private int BestNeural(double[] data, List<Neural> neurals)
        {
            double? maxValue = -10000000000;
            int maxIndex = -2;

            foreach (var neuron in neurals)
            {
                var value = neuron.Wzbudzenie(data);
                if (value > maxValue)
                {
                    maxValue = value;
                    maxIndex = neurals.IndexOf(neuron);

                }
            }

            return maxIndex;
        }


        //Test
        public List<double[]> Test(List<double[]> data)
        {
            List<double[]> outputs = new List<double[]>();
            for (int i = 0; i < data.Count; i++)
            {
                if (i % 5 == 0)
                {
                    var bestNeuron = BestNeural(data[i], Neurals);
                    var s = Neurals[bestNeuron].WzbudzenieNeurona;
                    outputs.Add(new[]{data[i][1], s, bestNeuron});
                }
            }

            return outputs;
        }
    }
}

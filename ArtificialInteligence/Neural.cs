﻿namespace ArtificialInteligence
{
    public class Neural
    {
        public double Alpha { get; set; }
        public double[] Weights { get; set; }
        public double[] Inputs { get; set; }
        public double WzbudzenieNeurona { get; set; }


        public Neural(double alpha, double[] weights)
        {
            Alpha = alpha;
            Weights = weights;
        }

        public double Wzbudzenie(double[] x)
        {
            var wzb = MyMatrix.Multiply(Weights, x);
            WzbudzenieNeurona = wzb;
            return wzb;
        }

        public void Aktualizuj(double[] x)
        {
            //x - weights
            var s = MyMatrix.Substract(x, Weights);
            var alp = MyMatrix.Multiply(s, Alpha);
            var news = MyMatrix.Add(s, alp);

            Weights = news;
        }
    }
}

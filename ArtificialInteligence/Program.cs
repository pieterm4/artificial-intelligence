﻿using System;
using System.Collections.Generic;
using System.Text;
using LoadFiles;

namespace ArtificialInteligence
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"iris.csv";
            LoadFromCsv file = new LoadFromCsv(path);

            file.LoadFromFile();
            var results = file.Table;

            WTA wta = new WTA(100000, 10, 4, 0.000001);

            //Wypisz wagi
            Console.WriteLine("Wagi przed uczeniem: ");
            foreach (var VARIABLE in wta.Neurals)
            {
                for (int i = 0; i < VARIABLE.Weights.GetLength(0); i++)
                {
                    Console.Write(VARIABLE.Weights[i].ToString("N10") + "\t");
                }
                Console.Write('\n');
            }


            //Normalizacja danych
            foreach (var res in results)
            {
                for (int i = 0; i < res.GetLength(0) - 1; i++)
                {
                    res[i] = MyMatrix.Normalize(results, -1, 1, res[i]);
                }
            }

            wta.Learn(10000, results);

            //Wypisz wagi po uczeniu

            Console.Write("\n\n\n");
            Console.WriteLine("Wagi po uczeniu: ");
            foreach (var VARIABLE in wta.Neurals)
            {
                for (int i = 0; i < VARIABLE.Weights.GetLength(0); i++)
                {
                    Console.Write(VARIABLE.Weights[i].ToString("N10") + "\t");
                }
                Console.Write('\n');
            }
            Console.Write("\n\n\n");
            Console.WriteLine("Testowanie: ");

            var test = wta.Test(results);

            Console.WriteLine("x\t\ty\n");
            foreach (var doublese in test)
            {
                StringBuilder str = new StringBuilder();
                str.Append(doublese[0].ToString("N10"));
                str.Append("\t");
                str.Append(doublese[1].ToString("N10"));
                str.Append("\t");
                str.Append(doublese[2]);

                Console.WriteLine(str);
            }

            //Zapis do pliku
            string outputPath = @"output.txt";
            Save(test, outputPath);
            Console.Read();
        }

        public static async void Save(List<double[]> result, string path)
        {
            SaveToTxt saveToTxt = new SaveToTxt(result);
            await saveToTxt.Save(path);
        }
    }


}

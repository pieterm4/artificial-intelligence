﻿using System.Threading.Tasks;

namespace LoadFiles.Interfaces
{
    public interface ISaveToFile
    {
        Task Save(string path);
    }
}

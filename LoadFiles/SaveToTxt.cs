﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using LoadFiles.Interfaces;

namespace LoadFiles
{
    public class SaveToTxt:ISaveToFile
    {
        public List<double[]> Output { get; set; }

        public SaveToTxt(List<double[]> output)
        {
            Output = new List<double[]>(output);
        }
        public async Task Save(string path)
        {
            using (var stream = new StreamWriter(path))
            {
                foreach (var line in Output)
                {
                    StringBuilder str = new StringBuilder();
                    str.Append(line[0].ToString("N10"));
                    str.Append("\t");
                    str.Append(line[1].ToString("N10"));
                    str.Append(line[2]);

                    await stream.WriteLineAsync(str.ToString());
                }
            }
        }
    }
}

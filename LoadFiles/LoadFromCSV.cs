﻿using System;
using System.Collections.Generic;
using System.IO;


namespace LoadFiles
{
    public class LoadFromCsv
    {
        public List<double[]> Table { get; set; }

        private readonly string _path;

        public LoadFromCsv(string path)
        {
            _path = path;
        }

        public void LoadFromFile()
        {
            List<string[]> ListA = new List<string[]>();
            using (var fs = File.OpenRead(_path))
            using (var reader = new StreamReader(fs))
            {
                
                

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    ListA.Add(values);
                    
                }
            }
            ListA.RemoveAt(ListA.Count - 1);
            Table = Convert(ListA);

        }

        private List<double[]> Convert(List<string[]> _lista)
        {
            var newList = new List<double[]>();
            foreach (var stringse in _lista)
            {
                double[] tmp = new double[4];
                for (int i = 0; i < 4; i++)
                {
                    tmp[i] = Double.Parse(stringse[i]);
                }
                newList.Add(tmp);
            }

            return newList;
        }

    }
}
